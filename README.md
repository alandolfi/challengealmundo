# ChallengeAlMundo

### Dependencias:
  - Node
  - NPM
  - Mongo
  
### Requisitos para levantar la aplicación

Es necesario instalar todas las dependencias de Node definidas en package.json
```
 cd path/hoteles-ui
 npm install && npm run start
 localhost:3000

```

```
 cd path/hoteles-backend
 npm install && npm run start
 localhost:3001
 swagger: localhost:3001/api-docs
```