import * as React from 'react';
import star from '../../assets/icons/filters/star.svg';
import { Image, Container } from './style';

const Stars: React.FunctionComponent<{
	count: number;
}> = props => {
	let starsImage = [];
	for (let i = 0; i < props.count; i++) starsImage.push(<Image key={i} src={star} />);

	return <Container>{starsImage}</Container>;
};

export default Stars;
