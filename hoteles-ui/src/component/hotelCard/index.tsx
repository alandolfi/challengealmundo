import * as React from 'react';
import Stars from '../stars';
import Amenities from '../amenities';
import {
	Container,
	HotelPriceContainer,
	HotelInformationContainer,
	Image,
	HotelTitle,
	HotelPriceDisclaimer,
	HotelPrice,
	HotelCurrencyPrice,
	HotelButtonView,
} from './style';

const HotelCard: React.FunctionComponent<{
	image: string;
	name: string;
	stars: number;
	amenities: string[];
	price: number;
	id: string;
	isMobile: boolean;
}> = props => {
	return (
		<Container isMobile={props.isMobile}>
			<Image src={`/images/hotels/${props.image}`} isMobile={props.isMobile} />

			<HotelInformationContainer isMobile={props.isMobile}>
				<HotelTitle> {props.name}</HotelTitle>
				<Stars count={props.stars} />
				<Amenities amenities={props.amenities} />
			</HotelInformationContainer>

			<HotelPriceContainer isMobile={props.isMobile}>
				<HotelPriceDisclaimer>Precio por noche por habitacion</HotelPriceDisclaimer>
				<HotelCurrencyPrice>ARS</HotelCurrencyPrice>
				<HotelPrice>{props.price}</HotelPrice>
				<HotelButtonView>Ver Hotel</HotelButtonView>
			</HotelPriceContainer>
		</Container>
	);
};

export default HotelCard;
