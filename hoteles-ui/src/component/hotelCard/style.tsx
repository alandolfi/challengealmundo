import styled from 'styled-components';
import { isMobile } from 'react-device-detect';
export const Container = styled.div<{ isMobile: boolean }>`
	width: 100%;
	height: 200px;
	margin: ${p => (p.isMobile ? '0px' : '0px 40px')};
	margin-bottom: 10px;
	background-color: white;
	display: flex;
	flex-wrap: wrap;
	align-items: center;
	justify-content: center;
	padding: 10px;
`;

export const Image = styled.img<{ isMobile: boolean }>`
	width: ${p => (p.isMobile ? '30%' : '300px')};
	height: 100%;
	background: red;
	flex: 1 1 auto;
`;

const Item = styled('div')`
	flex-grow: 1;
	flex: 1 1 auto;
	height: 100%;
`;

export const HotelInformationContainer = styled(Item)<{ isMobile: boolean }>`
	width: ${p => (p.isMobile ? '30%' : '50%')};
	padding-left: 10px;
`;

export const HotelPriceContainer = styled(Item)<{ isMobile: boolean }>`
	width: 20%;
	border-left: 2px dotted #c4c4c4;
	justify-content: center;
	align-items: center;
	text-align: center;
`;

export const HotelPrice = styled('label')`
	color: #df6800;
	font-size: 2rem;
	margin: 10px 5px;
	font-weight: 500;
`;

export const HotelCurrencyPrice = styled('label')`
	color: #df6800;
	font-weight: 300;
	font-size: 1.5rem;
`;

export const HotelTitle = styled('label')`
	color: #0579b3;
	font-size: 20px;
	font-weight: 500;
`;

export const HotelButtonView = styled('button')`
	padding: 10px 40px;
	border: 2px solid #002c77;
	border-radius: 3px;
	margin-top: 30px;
	background-color: #002c77;
	color: #fff;
	box-shadow: 0 -2px 0 #00163c inset;
	font-size: 1rem;
	font-weight: 600;
`;

export const HotelPriceDisclaimer = styled('small')`
	color: #686868;
	font-size: 0.8rem;
	display: flex;
	justify-content: center;
	margin-left: 20px;
	margin-right: 20px;
	margin-top: 30px;
	margin-bottom: 10px;
	padding-left: 10px;
`;
