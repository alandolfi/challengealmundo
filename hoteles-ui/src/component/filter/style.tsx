import styled from 'styled-components';
export const Container = styled('div')`
	margin-top: 10px;
`;

export const Image = styled('img')`
	width: 20px;
	height: 20px;
	margin: 0px 5px 5px 5px;
`;

export const ImageUp = styled(Image)`
	width: 30px;
	height: 30px;
	display: flex;
	margin-right: 25px;
`;

export const ContainerHeader = styled('div')`
	display: flex;
	padding: 20px;
	padding-bottom: 0px;
	padding-right: 0px;
	background: white;
	width: 100%;
	justify-content: space-between;
`;
export const Title = styled('div')`
	color: #0579b3;
	font-weight: 500;
	width: 100%;
	margin-left: 10px;
`;
