import React, { useState } from 'react';
import { ContainerHeader, Container, Image, Title, ImageUp } from './style';

const Filter: React.FunctionComponent<{
	titile: string;
	path: string;
}> = props => {
	const [toggle, setToggle] = useState(true);
	return (
		<Container>
			<ContainerHeader onClick={() => setToggle(!toggle)}>
				<Image src={props.path} />
				<Title>{props.titile}</Title>
				<ImageUp src={`/images/icons/filters/up.svg`} />
			</ContainerHeader>

			{toggle && props.children}
		</Container>
	);
};

export default Filter;
