import styled from 'styled-components';
export const Container = styled('div')`
	width: 100%;
	height: 80px;
	padding-left: 15px;
	background-color: #103476;
`;

export const Logo = styled('img')`
	width: 200px;
	height: 69px;
`;
