import * as React from 'react';
import { Container, Logo } from './style';
import LogoSvg from '../../logo-almundo.svg';

const Header: React.FunctionComponent<{}> = props => {
	return (
		<Container>
			<Logo src={LogoSvg} />
		</Container>
	);
};

export default Header;
