import * as React from 'react';
import { Image, Container } from './style';

const Amenities: React.FunctionComponent<{
	amenities: string[];
}> = props => {
	const amenitiesImage = [];
	for (let i = 0; i < props.amenities.length; i++) {
		amenitiesImage.push(<Image src={`/images/icons/amenities/${props.amenities[i]}.svg`} />);
	}

	return <Container>{amenitiesImage}</Container>;
};

export default Amenities;
