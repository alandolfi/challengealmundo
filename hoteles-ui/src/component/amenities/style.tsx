import styled from 'styled-components';
export const Container = styled('div')``;

export const Image = styled('img')`
	width: 20px;
	height: 20px;
	margin: 0px 5px 5px 5px;
`;
