import styled from 'styled-components';
import { isMobile } from 'react-device-detect';

export const Container = styled.div<{ isMobile: boolean }>`
	width: 100%;
	height: 100%;
	display: flex;
	margin: ${p => (p.isMobile ? '0px' : '30px')};
`;

export const ContainerFilter = styled.div<{ isMobile: boolean }>`
	width: 400px;
	display: ${p => (p.isMobile ? 'none' : 'block')};
`;

export const ContainerFilterSearch = styled('div')`
	width: 400px;
`;

export const ContainerHotelList = styled('div')`
	width: 100%;
`;

export const FilterTitle = styled('span')`
	width: 100%;
	height: 60px;
	color: #444;
	display: flex;
	padding-left: 18px;
	font-size: 18px;
	align-items: center;
	background: white;
	font-weight: 500;
`;

export const FilterItem = styled.div<{ hidePadding: number }>`
	width: 100%;
	background: white;
	display: flex;
	padding: ${p => (p.hidePadding == 0 ? '0px' : '20px')};
	padding-bottom: ${p => (p.hidePadding == 0 ? '0px' : '5px')};
	padding-right: 0px;
	padding-left: 20px;
	padding-bottom: 20px;
`;

export const FilterlButtonSearch = styled('button')`
	border: 2px solid #002c77;
	border-radius: 3px;
	margin-left: 10px;
	background-color: #002c77;
	color: #fff;
	box-shadow: 0 -2px 0 #00163c inset;
	font-size: 1rem;
	font-weight: 600;
`;

export const FilterlSearch = styled('input')`
	height: 30px;
`;

export const HotelList = styled.div<{ isMobile: boolean }>`
	width: ${p => (p.isMobile ? '100%' : '60%')};
`;
