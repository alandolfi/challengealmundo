import React, { useEffect, useState } from 'react';
import { isMobile } from 'react-device-detect';
import {
	Container,
	ContainerFilter,
	FilterTitle,
	FilterItem,
	FilterlButtonSearch,
	FilterlSearch,
	HotelList,
} from './style';
import axios from 'axios';
import Header from '../../component/header';
import HotelCard from '../../component/hotelCard';
import Filter from '../../component/filter';
import Start from '../../component/stars';

const Hotels: React.FunctionComponent = () => {
	const [hotels, setHotels] = useState([]);
	const [stars, setStars] = useState<any>([]);
	const [name, setName] = useState('');

	useEffect(() => {
		axios.get('http://localhost:3001/hotels').then(response => {
			setHotels(response.data);
		});
	}, []);

	const onSearch = () => {
		console.log(stars);
		axios.get(`http://localhost:3001/hotels?name=${name}&stars=${stars}`).then(response => {
			setHotels(response.data);
		});
	};

	const onCheckedStar = (isChecked: boolean, star: any) => {
		if (isChecked) {
			setStars([...stars, star]);
		} else {
			setStars(stars.filter((s: any) => s !== star));
		}
	};

	useEffect(() => {
		onSearch();
	}, [stars]);

	return (
		<>
			<Header />
			<Container isMobile={isMobile}>
				<ContainerFilter isMobile={isMobile}>
					<FilterTitle>Filtros</FilterTitle>
					<Filter titile="Nombre del Hotel" path="/images/icons/filters/search.svg">
						<FilterItem hidePadding={0}>
							<FilterlSearch
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								placeholder="ingrese nombre del hotel"
							/>
							<FilterlButtonSearch onClick={onSearch}>Aceptar</FilterlButtonSearch>
						</FilterItem>
					</Filter>

					<Filter titile="Estrellas" path="/images/icons/filters/star.blue.svg">
						<FilterItem hidePadding={5}>
							<input type="checkbox" onChange={e => onCheckedStar(e.target.checked, -1)} />
							<label>Todas las extrellas</label>
						</FilterItem>
						{[5, 4, 3, 2, 1].map(star => (
							<FilterItem hidePadding={5}>
								<input type="checkbox" onChange={e => onCheckedStar(e.target.checked, star)} />
								<Start count={star} />
							</FilterItem>
						))}
					</Filter>
				</ContainerFilter>
				<HotelList isMobile={isMobile}>
					{hotels.map(hotel => (
						<HotelCard {...hotel} isMobile={isMobile} />
					))}
				</HotelList>
			</Container>
		</>
	);
};

export default Hotels;
