import React from 'react';
import Hotels from './container/hotels/';

const App: React.FC = () => {
	return <Hotels />;
};

export default App;
