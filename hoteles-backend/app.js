const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const compression = require('compression');
const methodOverride = require('method-override');
const router = require('./router');
const config = require('config');

// Middlewares
app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(require('express-promise')());

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Methods', 'POST, GET, HEAD, OPTIONS, PUT,DELETE');
	res.header('Access-Control-Allow-Credentials', 'true');
	next();
});

router.setup(express)(app);

// Start server
app.listen(config.port, function() {
	console.log('Express server listening on %s:%d', config.server, config.port);
});
