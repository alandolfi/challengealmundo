const mongo = require('./mongo');
const data = require('../mocks/data.json');
const ObjectID = require('mongodb').ObjectID;

exports.add = hotel =>
	mongo.collection('hotels').then(col => {
		return col.insert(hotel);
	});

exports.update = (hotel, id) =>
	mongo.collection('hotels').then(col => {
		return col.findOneAndUpdate({ _id: new ObjectID(id) }, hotel).then(newHotel => hotel);
	});

exports.remove = id =>
	mongo.collection('hotels').then(col => {
		return col.remove({ _id: new ObjectID(id) });
	});

exports.getAll = filter =>
	new Promise((resolve, reject) => {
		if (filter['name'] || filter['stars']) {
			let dataFilter = [];
			if (filter['name']) {
				dataFilter = data.filter(d => d.name.toLowerCase().includes(filter['name'].toLowerCase()));
			}

			if (filter['stars']) {
				if (dataFilter.length === 0) dataFilter = data;
				const stars = filter['stars'].split(',').map(s => parseInt(s));

				dataFilter = dataFilter.filter(d => stars.includes(d.stars));
			}
			resolve(dataFilter);
		} else {
			resolve(data);
		}
	});

exports.getById = id =>
	mongo.collection('hotels').then(col => {
		return col.findOne({ _id: new ObjectID(id) });
	});
