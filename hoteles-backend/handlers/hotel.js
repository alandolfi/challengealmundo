const model = require('../models');

exports.findAllDocuments = function(req, res) {
	var filter = require('url').parse(req.url, true).query;

	model.hotel.getAll(filter).then(data => {
		res.status(200).jsonp(data);
	});
};

exports.findById = function(req, res) {
	console.log(req.params.id);
	model.hotel.getById(req.params.id).then(hotel => {
		res.status(200).jsonp(hotel);
	});
};

exports.addDocument = function(req, res) {
	model.hotel.add(req.body).then(data => {
		res.status(200).jsonp(data.ops[0]);
	});
};

exports.updateDocument = function(req, res) {
	model.hotel.update(req.body, req.params.id).then(hotel => {
		res.status(200).jsonp(hotel);
	});
};
exports.deleteDocument = function(req, res) {
	model.hotel.remove(req.params.id).then(resul => {
		res.status(200).jsonp(resul);
	});
};
